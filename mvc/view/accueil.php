<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="../../style/accueil_styles.css" />
		<title>Document</title>
	</head>
	<body>
	<h1 class="title">ACCUEIL</h1>
	<div class="container">
		<div class="innerContainer">
			<div class="innerTitle">Retrouvez les données par thematique :</div>
			<div class="firstLine-buttons">
				<button class="btn btn-red">
					Administration et Finances Publiques
				</button>
				<button class="btn btn-darkblue">Citoyeneté</button>
			</div>
			<div>
				<button class="btn btn-green">Environnement</button>
				<button class="btn btn-orange">Mobilité et Espace Public</button>
				<button class="btn btn-blue">Equipements, Services social</button>
			</div>
		</div>
	</div>
	</body>
</html>
