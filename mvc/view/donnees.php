<!DOCTYPE html>
<?php
	include('../model/sys_information.php');
?>

<html>
	<body>
		<table>
			<tr>
				<th>
					Identifiant
				</th>
				<th>
					Nom
				</th>
				<th>
					Valeur
				</th>
			</tr>
			<?php
				foreach($json as $une_ligne) {
					echo "<tr>";
					foreach($une_ligne as $key => $value) {
						if ($key == "id_information") {
							 echo "<td>$value</td>";
						}
						if ($key == "nom_information") {
							 echo "<td>$value</td>";
						}
						if ($key == "valeur_information") {
							 echo "<td>$value</td>";
						}
					}
					echo "</tr>";
				}
			?>
		</table>
	</body>
</html>